MORZE_DICT = {'A': '.-', 'B': '-...', 'C': '-.-.', 'D': '-..', 'E': '.', 'F': '..-.', 'G': '--.', 'H': '....', 'I': '..', 'J': '.---', 'K': '-.-', 'L': '.-..', 'M': '--', 'N': '-.', 'O': '---', 'P': '.--.', 'Q': '--.-', 'R': '.-.', 'S': '...', 'T': '-', 'U': '..-', 'V': '...-', 'W': '.--', 'X': '-..-', 'Y': '-.--', 'Z': '--..', '1': '.----', '2': '..---', '3': '...--', '4': '....-', '5': '.....', '6': '-....', '7': '--...', '8': '---..', '9': '----.', '0': '-----', ', ': '--..--', '.': '.-.-.-', '?': '..--..', '/': '-..-.', '-': '-....-', '(': '-.--.', ')': '-.--.-'}


def code_morze(text):
    text = text.upper()

    # Initialize the result as an empty string
    result = []

    # Loop through each character in the input text
    for char in text:
        if char == " ":
            # Ignore space characters
            continue
        elif char in MORZE_DICT:
            # Use the Morse code mapping if the character is in the dictionary
            result.append(MORZE_DICT[char])
        else:
            # If the character is not in the dictionary, append it as is
            result.append(char)

    # Join the Morse code letters with space characters to separate them
    result = ' '.join(result)

    return result
